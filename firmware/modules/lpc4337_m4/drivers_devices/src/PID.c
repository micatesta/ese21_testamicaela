/*  PID de Temperatura
 *
 * Copyright 2021,

 * Micaela Testa
 * mica.testa@gmail.com
 * Maestría en Ingeniería Biomédica
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include "PID.h"
#include "gpio.h"
#include "systemclock.h"
#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

float proportional(float error, float kp) {

  float u;

  u = kp * error;

  return (u);
}

float saturate(PID_t *PID, float u) {

  if (u >= PID->max_u) {
    u = PID->max_u;
  } else if (u <= PID->min_u) {
    u = PID->min_u;
  }

  return (u);
}

/*==================[internal functions declaration]=========================*/

float step_Proportional(float signal, PID_t *PID) {

  float error = 0;
  float u = 0;
  error = PID->set_point - signal;
  u = proportional(error, PID->Kp);
  u = saturate(PID, u);

  return (u);
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/*==================[end of file]============================================*/
