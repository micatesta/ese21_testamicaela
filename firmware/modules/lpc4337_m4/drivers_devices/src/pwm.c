/*  PWM para Calentador
 *
 * Copyright 2021,

 * Micaela Testa
 * mica.testa@gmail.com
 * Maestría en Ingeniería Biomédica
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[objetivos]=============================================*/

/* Driver para configurar el PWM. */

/*==================[inclusions]=============================================*/

#include "pwm.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/* Puntero a la estructura que almacena los parámetros de PWM. */
pwm_t *pwm_global = 0;

/*==================[internal functions declaration]=========================*/

/* Interrupción de timer que genera la señal de PWM. */

void PwmTimerIntr() {
  if (pwm_global->counter < pwm_global->duty) {
    GPIOOn(pwm_global->pin);
  } else {
    GPIOOff(pwm_global->pin);
  }

  if (pwm_global->counter >= pwm_global->max - 1) {
    pwm_global->counter = 0;
  } else {
    pwm_global->counter++;
  }
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/* Inicializa las funcionalidades necesarias para implementar un PWM por
 * software. Configura el timer y el pin de GPIO. */

void PwmInit(pwm_t *pwm) {
  pwm_global = pwm;
  GPIOInit(pwm->pin, GPIO_OUTPUT);
  pwm->timer.pFunc = &PwmTimerIntr;
  pwm->timer.timer = TIMER_C;
  pwm->timer.period = 5000;
  TimerInit(&pwm->timer);
  TimerStart(pwm->timer.timer);
}

/*==================[end of file]============================================*/
