/*

 * Arquitectura de Sistemas Embebidos
 * FIUNER - 2021
 * Autora: Testa, Micaela
 *
 * 24-09-2021
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the n	ames of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "ECG.h"       /* <= own header */

#include "analog_io.h"
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/

uint16_t last_adc_value;
bool select_ecg1 = 0;

/*==================[internal data definition]===============================*/


const uint8_t ECG1[]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};


const uint8_t ECG2[] = {
17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
17,17,17
};


void adc_interrupt() {
	AnalogInputRead(CH1, &last_adc_value);
	AnalogStartConvertion();
}

int j = 0;

void timer_interrupt() {
	uint8_t *ecg;
	uint16_t n_samples;
	if (select_ecg1) {
		ecg = &ECG1[0];
		n_samples = sizeof(ECG1);
	} else {
		ecg = &ECG2[0];
		n_samples = sizeof(ECG2);
	}

	j = (j + 1) % n_samples;
	uint32_t value = (uint32_t) ecg[j] * (uint32_t) last_adc_value;
	uint8_t trunc_value = value / 1024;
	
	UartSendByte(SERIAL_PORT_PC, &trunc_value);
}

analog_input_config adc_config = {
	.input = CH1,
	.mode = AINPUTS_SINGLE_READ,
	.pAnalogInput = &adc_interrupt
};

timer_config my_timer = {TIMER_A,10,&timer_interrupt};

serial_config UART_USB = {
	.baud_rate = 115200,
	.port = SERIAL_PORT_PC,
	.pSerial = NULL
};

void switch1_interrupt()
{
	select_ecg1 = 1;
}

void switch2_interrupt()
{
	select_ecg1 = 0;
}

/*==================[internal functions declaration]=========================*/



/*==================[external data definition]===============================*/



/*==================[external functions definition]==========================*/


void SisInit(void)
{
	LedsInit();
	
	AnalogInputInit(&adc_config);
	
    UartInit(&UART_USB);
    
    TimerInit(&my_timer);
	TimerStart(TIMER_A);

	SwitchesInit();
	SwitchActivInt(SWITCH_1, &switch1_interrupt);
	SwitchActivInt(SWITCH_2, &switch2_interrupt);
	
	AnalogStartConvertion();
}


int main(void)
{
	SisInit();
	
    while(1)
    {
    	/*Nada*/
    }
}

/*==================[end of file]============================================*/

