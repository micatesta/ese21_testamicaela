# Analizador de viscoelasticidad de un coágulo de sangre

Una prueba viscoelástica es un método de análisis hemostático que proporciona una visualización en tiempo real de la coagulación ex-vivo. Permite el examen de las contribuciones de las proteínas plasmáticas y celulares a la coagulación, incluido el número y la función de las plaquetas, la función de la fibrina y la función del factor de coagulación.

El método evalúa las propiedades físicas del coágulo durante la transición de la sangre de un estado líquido a un gel (sólido elástico), ya sea mediante la medición del módulo de corte del coágulo usando transducción de fuerza física o mediante la medición de la frecuencia de resonancia del coágulo usando interrogación sonométrica. Los resultados se informan en una traza en vivo, con diferentes parámetros de ésta que reflejan diferentes contribuyentes a la hemostasia. Estos parámetros informados varían entre las plataformas de prueba.

En el VILAS, dispositivo desarrollado por MZP Tecnología, se pretende evaluar la coagulación de la sangre a través de la lectura de la variación de presión de la muestra ante la aplicación de una excitación senoidal constante (ejercida mediante una jeringa). Se medirá la temperatura del sistema ya que la sangre debe mantenerse a 37ºC. 


**Objetivo de la aplicación**

- Aplicación de Sistemas embebidos con la plataforma [EDU-CIAA](www.proyecto-ciaa.com.ar/) basada en LPC4337JBD144. 

- Medir la presión de un coágulo de sangre al excitarlo mediante un movimiento constante.

- Medir la temperatura de la muestra durante su medición y controlarla mediante un PID. 


**Diagrama en bloques**

<img src="Diagrama_en_bloque.png"
     alt="Diagrama_en_bloque">

**Listado de periféricos a utilizar (marca, modelo y driver dispuesto)**

- Sensores de presión analógicos: PX26 SERIES Honeywell

- Termistores 100k

**<u> 1. Módulo de presión</u>**

La presión será medida a través de un sensor de la marca Omega, serie PX26. Este sensor es de tipo analógicos y esencialmente son puentes de Wheastone, con o sin compensación de temperatura. Uno de los puertos medirá la presión atmosférica, mientras que el otro sensará la presión del sistema. 

Para la medición del desbalance utilizamos un módulo basado en el chip HX711 de Avia Semiconductors. Este chip está especialmente diseñado para la medición de celdas de carga basadas en strain gages tipo puente de Wheastone. Consiste en un amplificador SigmaDelta de 24 bits, un voltaje de referencia y un amplificador de ganancia variable de instrumentación.
Los pines 1 y 3 del sensor de presión se conectan a los pines E+ y E- del amplificador y los pines 2 y 4 a A+ y A-.

Los pines digitales DAT y CLK están conectados a los pines T_COL0 y T_COL1 respectivamente. 

***Esquemático del módulo de lectura de presión***

<img src="Modulo_presion.png"
     alt="Modulo_presion"
     width="600" height="450">

**<u> 2. Módulo de temperatura</u>**

En el viscoelastómero es fundamental mantener la temperatura del sistema y de la muestra a 37ºC (temperatura de la sangre en el cuerpo). Para ello, se lee esta variable a través de un termistor conectado al canal analógico CH3. 

***Esquemático del módulo de lectura de temperatura***

<img src="Modulo_temperatura.png"
     alt="Modulo_temperatura"
     width="400" height="280">


El módulo de termalización se trata de una resistencia variable que, al modularle la corriente que pasa por medio de ella, su disipación en potencia aumenta y se logra generar calor.

Para lograr modular esta corriente, se generó un PWM a la salida del microprocesador que amplifica la señal por medio de un transistor 2N2222, utilizado en corte y saturación. A su vez, se encuentra configurado en la topologia Darlington, con un TIP29 para lograr los valores de potencia necesarios para manejar la corriente de consumo del calentador (o heater).

Por último, los valores del termistor son utilizados para controlar la temperatura a través de un PID que accionan el calentador. 

***Esquemático del módulo de lectura de termalización***

<img src="Modulo_calentador.png"
     alt="Modulo_calentador"
     width="800" height="500">

