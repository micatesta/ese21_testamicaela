/* Copyright 2021,
 * Micaela Testa
 * mica.testa@gmail.com
 * Maestría en Ingeniería Biomédica
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 */




/*==================[inclusions]=============================================*/



/*==================[macros and definitions]=================================*/

#define PWM_PIN GPIO_T_FIL0
#define PWM_COUNTER_MAX 1024

#define PID_KP 600
#define PID_SETPOINT 37
#define PID_MAX_U 1023
#define PID_MIN_U 0

#define UART_BAUDRATE 115200

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif


/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/




