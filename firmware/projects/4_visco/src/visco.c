/* Copyright 2021,
 * Micaela Testa
 * mica.testa@gmail.com
 * Maestría en Ingeniería Biomédica
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 *
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "visco.h"
#include "HX711.h"
#include "PID.h"
#include "pwm.h"
#include "switch.h"
#include "systemclock.h"
#include "temp.h"
#include "uart.h"
#include <chip.h>
#include <stdint.h>
//#include "i2c.h"
//#include "timer.h"

/*==================[macros and definitions]=================================*/

/* Variable de control para el envío por UART de datos */
int send_control = 0;

void switch1_interrupt() { send_control = 0; }

void switch2_interrupt() { send_control = 1; }

void switch3_interrupt() { send_control = 2; }

/*==================[internal data definition]===============================*/

/* Configuraciones del PID */
PID_t PID = {
    .Kp = PID_KP,
    .set_point = PID_SETPOINT,
    .max_u = PID_MAX_U,
    .min_u = PID_MIN_U,
};

/* Configuración del puerto serie */
serial_config UART_USB = {
    .baud_rate = UART_BAUDRATE, .port = SERIAL_PORT_PC, .pSerial = NULL};

/* Configuración del PWM */
pwm_t pwm = {.pin = PWM_PIN,
             .duty = 0,
             .max = PWM_COUNTER_MAX};

void report_por_uart(float temp_value, uint16_t u_int, int32_t presion) {
  uint8_t databyte = 0;
  if (send_control == 0) {
    /* Enviar duty cycle de PWM. */
    databyte = u_int & 0xFF;
    UartSendByte(SERIAL_PORT_PC, &databyte);
    u_int = u_int >> 8;
    databyte = u_int & 0xFF;
    UartSendByte(SERIAL_PORT_PC, &databyte);
  } else if (send_control == 1) {
    /* Enviar miligrados centígrados */
    int16_t temp = temp_value * 1000;
    databyte = temp & 0xFF;
    UartSendByte(SERIAL_PORT_PC, &databyte);
    temp = temp >> 8;
    databyte = temp & 0xFF;
    UartSendByte(SERIAL_PORT_PC, &databyte);
  } else if (send_control == 2) {
    /* Enviar presión */
    databyte = presion & 0xFF;
    UartSendByte(SERIAL_PORT_PC, &databyte);
    presion = presion >> 8;
    databyte = presion & 0xFF;
    UartSendByte(SERIAL_PORT_PC, &databyte);
    presion = presion >> 8;
    databyte = presion & 0xFF;
    UartSendByte(SERIAL_PORT_PC, &databyte);
    presion = presion >> 8;
    databyte = presion & 0xFF;
    UartSendByte(SERIAL_PORT_PC, &databyte);
  }
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

void SisInit(void) {
  /* Configura dispositivos */
  UartInit(&UART_USB);
  SystemClockInit();
  Temp_Init();
  HX711_init();
  PwmInit(&pwm);

  /* Configura switches para cambiar info enviada por UART */
  SwitchesInit();
  SwitchActivInt(SWITCH_1, &switch1_interrupt);
  SwitchActivInt(SWITCH_2, &switch2_interrupt);
  SwitchActivInt(SWITCH_3, &switch3_interrupt);
}

/*main*/

int main() {
  int32_t presion;
  float temp_value;
  uint16_t u_int;

  uint32_t send_uart = 0;

  SisInit();

  /* Ciclo principal */
  while (1) {
    /* Lectura de presión */
    presion = HX711_read();

    /* Lectura de valor de temperatura */
    temp_value = Temp_read();

    /* Cálculo de señal de control */
    u_int = step_Proportional(temp_value, &PID);

    /* Aplicar nuevo duty cycle */
    pwm.duty = u_int;

    /* Envío de datos por UART */
    send_uart++;
    if (true /*send_uart == 20000-1*/) {
    report_por_uart(temp_value, u_int + 33000, presion);
    send_uart = 0;
    }
  }

  /* No llegamos aquí */
  return (0);
}

/*==================[end of file]============================================*/
